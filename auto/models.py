from django.db import models

class AutoSlider(models.Model):
    name = models.CharField(max_length=255)
    text = models.TextField(default='')
    img = models.ImageField(upload_to='auto-slider/img/')
    background_img = models.ImageField(upload_to='auto-slider/img/back/')



class AutoContacts(models.Model):
    address = models.TextField(default="")
    email = models.TextField(default="")

class AutoAddress(models.Model):
    address = models.TextField(default="")

class AutoEmail(models.Model):
    email = models.TextField(default="")