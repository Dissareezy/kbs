window.onload = () => {
  const swiper = new Swiper('.swiper1', {
    loop: true,
    slidesPerView: 1,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      renderBullet: function (index, className) {
        return '<span class="' + className + '">' + 0 + (index + 1) + '</span>';
      },
    },
  });
  const swiper2 = new Swiper('.swiper2', {
    loop: true,

    pagination: {
      el: '.swiper-pagination-swiper2',
      clickable: true,
    },
  });
  const swiper3 = new Swiper('.swiper3', {
    loop: true,
    slidesPerView: 1,
    spaceBetween: 10,
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      // when window width is >= 640px
      640: {
        slidesPerView: 3,
        spaceBetween: 30
      },
      968: {
        slidesPerView: 4,
        spaceBetween: 40
      }
    },

    pagination: {
      el: '.swiper-pagination-swiper3',
      clickable: true,
    },
  });
  const swiper4 = new Swiper('.swiper4', {
    loop: true,

    slidesPerView: 1,
    spaceBetween: 5,
    
    pagination: {
      el: '.swiper-pagination-swiper4',
      clickable: true,
    },
  });
  const swiper5 = new Swiper('.swiper5', {
    loop: true,

    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      968: {
        slidesPerView: 2,
        spaceBetween: 25
      }
    },

    pagination: {
      el: '.swiper-pagination-swiper5',
      clickable: true,
    },
  });
};


var prevScrollpos = window.pageYOffset;
window.onscroll = function(){
    var currentScrollPos = window.pageYOffset;
    if(prevScrollpos > currentScrollPos){
        document.getElementById("navbar").style.top = "0px";
    }else if(currentScrollPos == 0){
        document.getElementById("navbar").style.top = "0px";
    }else{
        document.getElementById("navbar").style.top = "-130px";
    }
    prevScrollpos = currentScrollPos;
    
var scrolled;
    scrolled = window.pageYOffset || document.documentElement.scrollTop;
        if(scrolled > 10){
            $("#navbar").addClass('active');
            $("#navbar").removeClass('nonactive');
        }
        if(10 > scrolled){
            $("#navbar").addClass('nonactive');
            $("#navbar").removeClass('active'); 
        }
}

$(function() {
  $(document).on("click", ".mob-toggle", function(e) {
    $(this).toggleClass("active");
    $(".m-nav").toggleClass("active");
    if ( $(".mob-toggle").hasClass("active")) {
    $("body").css("overflow", "hidden");
    } else {
      $("body").css("overflow", "auto");
    }
  });
})


$(function() {
  $(document).on("click", ".m-nav a", function(e) {
    $(".mob-toggle").removeClass("active");
    $(".m-nav").removeClass("active");
    $("body").css("overflow", "auto");
  });
})


let product = document.querySelectorAll('.product');
let strelka = document.querySelectorAll('.strelka');

product.forEach(function(item, i) {
  product[i].addEventListener('mouseover', function() {
    if(product[4] || product[6]) {
      strelka[4].style.cssText = 'padding-top: 25px;';
      strelka[6].style.cssText = 'padding-top: 25px;';
    }
    strelka[i].style.opacity = '100%';
    product[i].style.cssText = 'border: 0px; box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.25);';
  });
  product[i].addEventListener('mouseout', function() {
    strelka[i].style.opacity = '0%';
    product[i].style.cssText = 'border: 1px solid #465998; box-shadow: 0px 0px 0px rgba(0, 0, 0, 0.25);';
  });
});

let click = document.querySelectorAll('.click');
let number = document.querySelectorAll('.numer');
let numberActive = document.querySelectorAll('.number-active');

click.forEach(function(item, i) {
  click[i].addEventListener('click', function() {
    number[i].style.cssText = 'display: none;';
    numberActive[i].style.cssText = 'display: block;';
  });
});

var element = document.getElementById('tel');
var maskOptions = {
  mask: '+{7}(000)000-00-00'
};
var mask = IMask(element, maskOptions);
