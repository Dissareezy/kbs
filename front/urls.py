from django.urls import path, include
from .views import *
urlpatterns = [
    path('', FrontPageView, name='FrontPage'),

    path('services', ServicesView, name='front-services'),
    path('service-item/<int:service_id>',ServiceItemView,name='front-service-item'),

    path('news', NewsView, name='front-news'),
    path('news-item/<int:new_id>',newsItemView,name='front-news-item'),

    path('about', AboutView, name='about'),
    path('contacts', ContactsView, name='front-contacts'),

    path('mailer',mailerView,name='mailer')
]