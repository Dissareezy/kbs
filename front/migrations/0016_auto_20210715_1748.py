# Generated by Django 3.0.4 on 2021-07-15 11:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('front', '0015_auto_20210712_1859'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='description_en',
            field=models.TextField(default='', null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='description_kk',
            field=models.TextField(default='', null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='description_ru',
            field=models.TextField(default='', null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='name_en',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='name_kk',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='name_ru',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
