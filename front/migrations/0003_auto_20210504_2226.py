# Generated by Django 3.0.4 on 2021-05-04 16:26

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('front', '0002_services'),
    ]

    operations = [
        migrations.AlterField(
            model_name='services',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(),
        ),
    ]
