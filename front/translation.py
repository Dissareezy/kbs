from modeltranslation.translator import register, TranslationOptions
from .models import *

@register(Services)
class ServiceTranslationOptions(TranslationOptions):
    fields = ('name','description','text')

@register(SliderInfo)
class SliderTranslationOptions(TranslationOptions):
    fields = ('name','text')

@register(News)
class NewsTranslationOptions(TranslationOptions):
    fields = ('name','description','text')

@register(Project)
class ProjectTranslationOptions(TranslationOptions):
    fields = ('name','description')