from django.db import models

class SliderInfo(models.Model):
    name = models.CharField(max_length=255)
    text = models.TextField(default='')
    img = models.ImageField(upload_to='slider/img/')
    background_img = models.ImageField(upload_to='slider/img/back/')
