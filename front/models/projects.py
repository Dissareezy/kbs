from django.db import models

class Project(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    avatar = models.ImageField(upload_to='project/images/avatar/')
    description = models.TextField(default='')
    name = models.CharField(max_length=255)

class ProjectImage(models.Model):
    project = models.ForeignKey(Project,
                                on_delete=models.CASCADE,
                                related_name='project_images',
                                )
    image = models.ImageField(upload_to='project/images/')