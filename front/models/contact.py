from django.db import models

class Address(models.Model):
    address = models.TextField(default="")

class Phone(models.Model):
    phone = models.TextField(default="")

    def get_excerpt(self):
        return self.phone[:9]  if len(self.phone) > 9 else self.phone
class Email(models.Model):
    email = models.TextField(default="")

class Worktime(models.Model):
    holidaytime = models.TextField(default="")

class Contact(models.Model):
    address = models.ForeignKey(Address,
                                on_delete=models.CASCADE,
                                related_name="c_ad"
                                )
    phone = models.ForeignKey(Phone,
                                on_delete=models.CASCADE,
                                related_name="c_ph"
                                )
    email = models.ForeignKey(Email,
                                on_delete=models.CASCADE,
                                related_name="c_em"
                                )


