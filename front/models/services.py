from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from ckeditor.fields import RichTextField

class Services(models.Model):
    name = models.CharField(max_length=255)
    img = models.ImageField(upload_to='services/img/')
    description = models.TextField(default='')
    text = RichTextField(blank=True,null=True)

    def get_excerpt(self):
        return self.text[:70] + "..." if len(self.text) > 70 else self.text