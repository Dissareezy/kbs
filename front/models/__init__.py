from .slider import *
from .services import *
from .projects import *
from .news import *
from .trust import *
from .contact import *

