from django.db import models
from ckeditor.fields import RichTextField

class News(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    avatar = models.ImageField(upload_to='project/images/avatar/')
    description = models.TextField(default='')
    name = models.CharField(max_length=255)
    text = RichTextField(blank=True,null=True)