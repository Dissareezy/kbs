from django.shortcuts import render,redirect
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from ..models import *

def AboutView(request):
    services = Services.objects.all()
    sliders = SliderInfo.objects.all()
    projects = Project.objects.all()
    trusted = Trust.objects.all()
    news = News.objects.all()
    address = Address.objects.all()
    phone = Phone.objects.all()
    email = Email.objects.all()
    context = {
        'sliders': sliders,
        'news': news,
        'projects': projects,
        'trusted': trusted,
        'services': services,
        'address': address,
        'phone': phone,
        'email': email,
    }
    return render(request,'front/about.html',context)
def ContactsView(request):
    services = Services.objects.all()
    sliders = SliderInfo.objects.all()
    projects = Project.objects.all()
    trusted = Trust.objects.all()
    news = News.objects.all()
    address = Address.objects.all()
    phone = Phone.objects.all()
    email = Email.objects.all()
    worktime = Worktime.objects.all()
    context = {
        'sliders': sliders,
        'news': news,
        'projects': projects,
        'trusted': trusted,
        'services': services,
        'address': address,
        'phone': phone,
        'email': email,
        'worktime': worktime,
    }
    return render(request,'front/contacts.html',context)

def mailerView(request):
    if request.method == 'POST':
        type = request.POST['form']
        if type == 'mail_1':
            name = request.POST['name']
            phone = request.POST['phone']
            email_form = request.POST['email']
            email = EmailMessage(
                'Заявка с сайта KBS на бесплатную консультацию',
                '1. ' + str(name) + '\n' +
                '2. ' + str(phone) + '\n' +
                '3. ' + str(email_form) + '\n'
                ,
                'kbsmailer@mail.ru',
                ['dias2001@inbox.ru']
            )
            email.send()
            return redirect('FrontPage')
        if type == 'mail_2':
            name = request.POST['name']
            phone = request.POST['phone']
            email = EmailMessage(
                'Заявка с сайта KBS ',
                '1. ' + str(name) + '\n' +
                '2. ' + str(phone) + '\n'
                ,
                'kbsmailer@mail.ru',
                ['dias2001@inbox.ru']
            )
            email.send()
            return redirect('FrontPage')
    return redirect('FrontPage')