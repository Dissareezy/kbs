from django.shortcuts import render,redirect,get_object_or_404
from ..models import *


def NewsView(request):
    services = Services.objects.all()
    sliders = SliderInfo.objects.all()
    projects = Project.objects.all()
    trusted = Trust.objects.all()
    news = News.objects.all()
    address = Address.objects.all()
    phone = Phone.objects.all()
    email = Email.objects.all()
    context = {
        'sliders': sliders,
        'news': news,
        'projects': projects,
        'trusted': trusted,
        'services': services,
        'address': address,
        'phone': phone,
        'email': email,
    }
    return render(request,'front/news.html',context)

def newsItemView(request,new_id):
    new = get_object_or_404(News,pk=new_id)
    services = Services.objects.all()
    sliders = SliderInfo.objects.all()
    projects = Project.objects.all()
    trusted = Trust.objects.all()
    news = News.objects.all()
    address = Address.objects.all()
    phone = Phone.objects.all()
    email = Email.objects.all()
    context = {
        'new':new,
        'sliders': sliders,
        'news': news,
        'projects': projects,
        'trusted': trusted,
        'services': services,
        'address': address,
        'phone': phone,
        'email': email,
    }
    return render(request,'front/news-item.html',context)