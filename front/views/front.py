from django.shortcuts import render
from ..models import *

def FrontPageView(request):
    services = Services.objects.all()
    sliders = SliderInfo.objects.all()
    if request.LANGUAGE_CODE == 'ru':
        sliders = sliders.exclude(text_ru='')
    elif request.LANGUAGE_CODE == 'en':
        sliders = sliders.exclude(text_en='')
    elif request.LANGUAGE_CODE == 'kk':
        sliders = sliders.exclude(text_kk='')
    projects = Project.objects.all()
    if request.LANGUAGE_CODE == 'ru':
        projects = projects.exclude(description_ru='')
    elif request.LANGUAGE_CODE == 'en':
        projects = projects.exclude(description_en='')
    elif request.LANGUAGE_CODE == 'kk':
        projects = projects.exclude(description_kk='')
    trusted = Trust.objects.all()
    news = News.objects.all()
    if request.LANGUAGE_CODE == 'ru':
        news = news.exclude(text_ru='')
    elif request.LANGUAGE_CODE == 'en':
        news = news.exclude(text_en='')
    elif request.LANGUAGE_CODE == 'kk':
        news = news.exclude(text_kk='')
    address = Address.objects.all()
    phone = Phone.objects.all()
    email = Email.objects.all()
    context = {
        'sliders': sliders,
        'news': news,
        'projects': projects,
        'trusted': trusted,
        'services': services,
        'address': address,
        'phone': phone,
        'email': email,
    }
    return render(request,'front/index.html',context)