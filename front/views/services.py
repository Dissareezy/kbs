from django.shortcuts import render,redirect,get_object_or_404
from ..models import *

def ServicesView(request):
    services = Services.objects.all()
    sliders = SliderInfo.objects.all()
    projects = Project.objects.all()
    trusted = Trust.objects.all()
    news = News.objects.all()
    address = Address.objects.all()
    phone = Phone.objects.all()
    email = Email.objects.all()
    context = {
        'sliders': sliders,
        'news': news,
        'projects': projects,
        'trusted': trusted,
        'services': services,
        'address': address,
        'phone': phone,
        'email': email,
    }
    return render(request,'front/services.html',context)

def ServiceItemView(request,service_id):
    service = get_object_or_404(Services,pk=service_id)
    services = Services.objects.all()
    sliders = SliderInfo.objects.all()
    projects = Project.objects.all()
    trusted = Trust.objects.all()
    news = News.objects.all()
    address = Address.objects.all()
    phone = Phone.objects.all()
    email = Email.objects.all()
    context = {
        'service': service,
        'sliders': sliders,
        'news': news,
        'projects': projects,
        'trusted': trusted,
        'services': services,
        'address': address,
        'phone': phone,
        'email': email,
    }
    return render(request,'front/service-item.html',context)