from django.db import models

# Create your models here.
class PlastContacts(models.Model):
    address = models.TextField(default="")
    email = models.TextField(default="")


class APlastAddress(models.Model):
    address = models.TextField(default="")

class PlastEmail(models.Model):
    email = models.TextField(default="")
