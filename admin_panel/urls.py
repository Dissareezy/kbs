from django.urls import path
from .views import *


urlpatterns = [
    path('', adminPanelView, name='panel'),

    path('slider-add', SliderAdd, name='slider-add'),
    path('slider-edit/<int:slider_id>', SliderEdit, name='slider-edit'),

    path('service-add', ServiceAddView, name='service-add'),
    path('service-edit/<int:service_id>', ServiceEditView, name='service-edit'),

    path('project-add', ProjectAddView, name='project-add'),
    path('project-edit/<int:project_id>', ProjectEditView, name='project-edit'),

    path('news-add', NewsAddView, name='news-add'),
    path('news-edit/<int:new_id>', newsEditView, name='news-edit'),

    path('trust-add', TrustAddView, name='trust-add'),

    path('contacts', ContactsView, name='admin-contacts'),
    path('contacts-edit/<int:contact_id>', ConctacsEditView, name='contacts-edit'),

    path('login', loginView, name='login'),
    path('logout', logoutView, name='logout'),

    path('modules-slider',ModulesSliderView,name='modules-slider-add'),
    path('modules-slider-edit/<int:slider_id>',ModulesSliderEditView,name='modules-slider-edit'),
    path('modules-contacts', ModulesContactsView, name='modules-contacts'),
    path('modules-contacts-edit/<int:contact_id>', ModulesContactsEditView, name='modules-contacts-edit'),

    path('auto-slider',autoSliderView,name='auto-slider-add'),
    path('auto-slider-edit/<int:slider_id>',autoSliderEditView,name='auto-slider-edit'),
    ]