from django import forms

from front.models import Services
from front.models import News

class PostForm(forms.ModelForm):
    class Meta:
        model = Services
        fields = ('name','text',)

class NewsForm(forms.ModelForm):
    class Meta:
        model = News
        fields = ('name','text',)