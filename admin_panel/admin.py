from django.contrib import admin
from front.models import *

from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms

# Register your models here.
admin.site.register(Services)
admin.site.register(SliderInfo)
admin.site.register(News)




class PostForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorUploadingWidget())