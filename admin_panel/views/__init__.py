from .slider import *
from .mainview import *
from .services import *
from .projects import *
from .news import *
from .stuff import *
from .auth import *
from .modules import *
from .auto import *

