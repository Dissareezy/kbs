from django.shortcuts import render,redirect,get_object_or_404
from front.models import *
from django.contrib.auth.decorators import login_required

@login_required
def TrustAddView(request):
    trusted = Trust.objects.all()
    if request.method == 'POST':
        type = request.POST['form']
        if type == 'add':
            logo = request.FILES.get('img')
            Trust.objects.create(logo=logo)
            return redirect('trust-add')
        if type == 'delete':
            trust_pk = request.POST['trust_pk']
            trust = get_object_or_404(Trust,pk=trust_pk)
            trust.delete()
            return redirect('trust-add')
    context = {
        'trusted':trusted,
    }
    return render(request,'admin_panel/trust-add.html',context)


@login_required
def ContactsView(request):
    contacts = Contact.objects.all()
    address = Address.objects.all()
    phone = Phone.objects.all()
    email = Email.objects.all()
    worktime = Worktime.objects.all()
    if request.method == 'POST':
        type = request.POST['type']
        if type == 'address':
            address = request.POST['address']
            Address.objects.create(address=address)
            return redirect('admin-contacts')
        if type == 'phone':
            phone = request.POST['phone']
            Phone.objects.create(phone=phone)
            return redirect('admin-contacts')
        if type == 'email':
            email = request.POST['email']
            Email.objects.create(email=email)
            return redirect('admin-contacts')
        if type == 'worktime':
            holidaytime = request.POST['worktime']
            Worktime.objects.create(holidaytime=holidaytime)
            return redirect('admin-contacts')
    context = {
        'contacts': contacts,
        'address': address,
        'email': email,
        'phone': phone,
        'worktime': worktime,
    }
    return render(request, 'admin_panel/admin-contacts.html', context=context)

@login_required
def ConctacsEditView(request,contact_id):
    if request.method == 'POST':
        type = request.POST['form']
        if type == 'address-edit':
            address = request.POST['address']
            ad_obj = get_object_or_404(Address,pk=contact_id)
            ad_obj.address=address
            ad_obj.save()
            return redirect('admin-contacts')
        if type == 'phone-edit':
            phone = request.POST['phone']
            ad_obj = get_object_or_404(Phone,pk=contact_id)
            ad_obj.phone=phone
            ad_obj.save()
            return redirect('admin-contacts')
        if type == 'email-edit':
            email = request.POST['email']
            ad_obj = get_object_or_404(Email,pk=contact_id)
            ad_obj.email=email
            ad_obj.save()
            return redirect('admin-contacts')
        if type == 'worktime-edit':
            worktime = request.POST['holidaytime']
            ad_obj = get_object_or_404(Worktime,pk=contact_id)
            ad_obj.holidaytime=worktime
            ad_obj.save()
            return redirect('admin-contacts')
        if type == 'address-delete':
            address_pk = request.POST['address_pk']
            address = get_object_or_404(Address,pk=address_pk)
            address.delete()
        if type == 'phone-delete':
            phone_pk = request.POST['phone_pk']
            phone = get_object_or_404(Phone,pk=phone_pk)
            phone.delete()
        if type == 'email-delete':
            email_pk = request.POST['email_pk']
            email = get_object_or_404(Email,pk=email_pk)
            email.delete()
        if type == 'worktime-delete':
            worktime_pk = request.POST['worktime_pk']
            email = get_object_or_404(Worktime,pk=worktime_pk)
            email.delete()

    context = {
    }
    return redirect('admin-contacts')