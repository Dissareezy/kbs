from django.shortcuts import render,redirect,get_object_or_404
from front.models import *
from ckeditor.widgets import forms
from ..forms import *
from django.contrib.auth.decorators import login_required
from django.utils.translation import *
@login_required
def ServiceAddView(request):
    current_language = get_language()
    services = Services.objects.all()
    if request.method == 'POST':
        name = request.POST['name']
        text = request.POST['text']
        description = request.POST['description']
        img = request.FILES.get('img')
        if current_language == 'kk':
            Services.objects.create(name_kk=name,text_kk=text,description_kk=description,img=img)
        if current_language == 'en':
            Services.objects.create(name_en=name, text_en=text, description_en=description, img=img)
        if current_language == 'ru':
            Services.objects.create(name_ru=name,text_ru=text,description_ru=description,img=img)
        return redirect('service-add')
    context = {
        'services':services,
        'form': PostForm,
    }
    return render(request,'admin_panel/service-add.html',context)

@login_required
def ServiceEditView(request,service_id):
    current_language = get_language()
    service = get_object_or_404(Services,pk=service_id)
    if request.method == 'POST':
        type = request.POST['form']
        if type == 'edit':
            name = request.POST['name']
            text = request.POST['text']
            description = request.POST['description']
            img = request.FILES.get('img')
            if img == None:
                if current_language == 'kk':
                    service.name_kk = name
                    service.text_kk = text
                    service.description_kk = description
                    service.save()
                    return redirect('service-edit',service_id)
                if current_language == 'en':
                    service.name_en = name
                    service.text_en = text
                    service.description_en = description
                    service.save()
                    return redirect('service-edit',service_id)
                if current_language == 'ru':
                    service.name_ru = name
                    service.text_ru = text
                    service.description_ru = description
                    service.save()
                    return redirect('service-edit',service_id)
            else:
                if current_language == 'kk':
                    service.name_kk = name
                    service.text_kk = text
                    service.description_kk = description
                    service.img = img
                    service.save()
                    return redirect('service-edit', service_id)
                if current_language == 'en':
                    service.name_en = name
                    service.text_en = text
                    service.description_en = description
                    service.img = img
                    service.save()
                    return redirect('service-edit', service_id)
                if current_language == 'ru':
                    service.name_ru = name
                    service.text_ru = text
                    service.description_ru = description
                    service.img = img
                    service.save()
                    return redirect('service-edit', service_id)
        if type == 'delete':
            service.delete()

            return redirect('service-add')
    context ={
        'service':service,
        'form':PostForm,
    }
    return render(request,'admin_panel/service-edit.html',context=context)