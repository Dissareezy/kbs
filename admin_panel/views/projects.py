from django.shortcuts import render,redirect,get_object_or_404
from front.models import *
from django.contrib.auth.decorators import login_required
from django.utils.translation import *

@login_required
def ProjectAddView(request):
    current_language = get_language()
    projects = Project.objects.all()
    if request.method == 'POST':
        avatar = request.FILES.get('avatar')
        description = request.POST['description']
        name = request.POST['name']
        if current_language == 'kk':
            Project.objects.create(name_kk=name,description_kk=description,avatar=avatar)
        if current_language == 'en':
            Project.objects.create(name_en=name,description_en=description,avatar=avatar)
        if current_language == 'ru':
            Project.objects.create(name_ru=name,description_ru=description,avatar=avatar)
        return redirect('project-add')

    context = {
        'projects': projects,
    }
    return render(request,'admin_panel/project-add.html',context)

@login_required
def ProjectEditView(request,project_id):
    current_language = get_language()
    project = get_object_or_404(Project,pk=project_id)
    if request.method == 'POST':
        type = request.POST['form']
        if type == 'edit':
            description = request.POST['description']
            name = request.POST['name']
            if current_language == 'kk':
                project.description_kk = description
                project.name_kk = name
                project.save()
                return redirect('project-edit',project_id)
            if current_language == 'en':
                project.description_en = description
                project.name_en = name
                project.save()
                return redirect('project-edit',project_id)
            if current_language == 'ru':
                project.description_ru = description
                project.name_ru = name
                project.save()
                return redirect('project-edit',project_id)
        if type == 'delete':
            project.delete()
            return redirect('project-add')
        if type =='add_img':
            img = request.FILES.get('img')
            ProjectImage.objects.create(project=project,image=img)

            return redirect('project-edit',project_id)
        if type == 'set_avatar':
            img_pk = request.POST['img_pk']
            avatar = ProjectImage.objects.get(pk=int(img_pk))
            project.avatar = avatar.image
            project.save()
            return redirect('project-edit',project_id)
    context = {
        'project': project,
    }
    return render(request,'admin_panel/project-edit.html',context)