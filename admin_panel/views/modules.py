from django.shortcuts import render,redirect,get_object_or_404
from modules.models import *

def ModulesSliderView(request):
    sliders = ModulesSlider.objects.all()
    if request.method == 'POST':
        name = request.POST['name']
        text = request.POST['text']
        img = request.FILES.get('img')
        back = request.FILES.get('back')
        ModulesSlider.objects.create(name=name, text=text, img=img, background_img=back)
        return redirect('modules-slider-add')
    context = {
        'sliders': sliders,
    }
    return render(request,'admin_panel/modules/slider-add.html',context)

def ModulesSliderEditView(request,slider_id):
    slider = get_object_or_404(ModulesSlider, pk=slider_id)
    if request.method == 'POST':
        type = request.POST['form']
        if type == 'edit':
            name = request.POST['name']
            text = request.POST['text']
            img = request.FILES.get('img')
            background_img = request.FILES.get('back')
            if img == None and background_img == None:
                slider.name = name
                slider.text = text
                slider.save()
                return redirect('modules-slider-edit', slider_id)
            elif img == None and background_img != None:
                slider.name = name
                slider.text = text
                slider.background_img = background_img
                slider.save()
                return redirect('modules-slider-edit', slider_id)
            elif img != None and background_img == None:
                slider.name = name
                slider.text = text
                slider.img = img
                slider.save()
                return redirect('modules-slider-edit', slider_id)
            elif img != None and background_img != None:
                slider.name = name
                slider.text = text
                slider.img = img
                slider.background_img = background_img
                slider.save()
                return redirect('modules-slider-edit', slider_id)
        if type == 'delete':
            slider.delete()
            return redirect('modules-slider-add')
    context = {
        'slider': slider,
    }
    return render(request,'admin_panel/modules/slider-edit.html',context)


def ModulesContactsView(request):
    contacts = ModContacts.objects.all()
    phone = ModPhone.objects.all()
    address = ModAddress.objects.all()
    email = ModEmail.objects.all()
    if request.method == 'POST':
        type = request.POST['type']
        if type == 'address':
            address = request.POST['address']
            ModAddress.objects.create(address=address)
            return redirect('modules-contacts')
        if type == 'phone':
            phone = request.POST['phone']
            ModPhone.objects.create(phone=phone)
            return redirect('modules-contacts')
        if type == 'email':
            email = request.POST['email']
            ModEmail.objects.create(email=email)
            return redirect('modules-contacts')
    context = {
        'contacts': contacts,
        'address': address,
        'email': email,
        'phone': phone,
    }
    return render(request,'admin_panel/modules/admin-contacts.html',context)

def ModulesContactsEditView(request,contact_id):
    if request.method == 'POST':
        type = request.POST['form']
        if type == 'address-edit':
            address = request.POST['address']
            ad_obj = get_object_or_404(ModAddress,pk=contact_id)
            ad_obj.address=address
            ad_obj.save()
            return redirect('admin-contacts')
        if type == 'phone-edit':
            phone = request.POST['phone']
            ad_obj = get_object_or_404(ModPhone,pk=contact_id)
            ad_obj.phone=phone
            ad_obj.save()
            return redirect('admin-contacts')
        if type == 'email-edit':
            email = request.POST['email']
            ad_obj = get_object_or_404(ModEmail,pk=contact_id)
            ad_obj.email=email
            ad_obj.save()
            return redirect('admin-contacts')
        if type == 'address-delete':
            address_pk = request.POST['address_pk']
            address = get_object_or_404(ModAddress,pk=address_pk)
            address.delete()
        if type == 'phone-delete':
            phone_pk = request.POST['phone_pk']
            phone = get_object_or_404(ModPhone,pk=phone_pk)
            phone.delete()
        if type == 'email-delete':
            email_pk = request.POST['email_pk']
            email = get_object_or_404(ModEmail,pk=email_pk)
            email.delete()
    context = {
    }
    return redirect('modules-contacts')