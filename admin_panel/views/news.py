from django.shortcuts import render,redirect,get_object_or_404
from front.models import *
from ..forms import *
from django.contrib.auth.decorators import login_required
from django.utils.translation import *


@login_required
def NewsAddView(request):
    current_language = get_language()
    news = News.objects.all()
    if request.method == 'POST':
        avatar = request.FILES.get('avatar')
        description = request.POST['description']
        text = request.POST['text']
        name = request.POST['name']
        if current_language == 'kk':
            News.objects.create(name_kk=name, text_kk=text, description_kk=description, avatar=avatar)
        if current_language == 'en':
            News.objects.create(name_en=name, text_en=text, description_en=description, avatar=avatar)
        if current_language == 'ru':
            News.objects.create(name_ru=name, text_ru=text, description_ru=description, avatar=avatar)
        return redirect('news-add')
    context = {
        'news': news,
        'form': NewsForm,
    }
    return render(request,'admin_panel/news-add.html',context)

@login_required
def newsEditView(request,new_id):
    current_language = get_language()
    new = get_object_or_404(News,pk=new_id)
    if request.method == 'POST':
        type = request.POST['form']
        if type == 'edit':
            description = request.POST['description']
            text = request.POST['text']
            name = request.POST['name']
            avatar = request.FILES.get('img')
            if avatar == None:
                if current_language == 'kk':
                    new.description_kk = description
                    new.name_kk = name
                    new.text_kk = text
                    new.save()
                    return redirect('news-edit',new_id)
                if current_language == 'en':
                    new.description_en = description
                    new.name_en = name
                    new.text_en = text
                    new.save()
                    return redirect('news-edit',new_id)
                if current_language == 'ru':
                    new.description_ru = description
                    new.name_ru = name
                    new.text_ru = text
                    new.save()
                    return redirect('news-edit',new_id)
            else:
                if current_language == 'kk':
                    new.description_kk = description
                    new.name_kk = name
                    new.text_kk = text
                    new.avatar = avatar
                    new.save()
                    return redirect('news-edit',new_id)
                if current_language == 'en':
                    new.description_en = description
                    new.name_en = name
                    new.text_en = text
                    new.avatar = avatar
                    new.save()
                    return redirect('news-edit',new_id)
                if current_language == 'ru':
                    new.description_ru = description
                    new.name_ru = name
                    new.text_ru = text
                    new.avatar = avatar
                    new.save()
                    return redirect('news-edit',new_id)
        if type == 'delete':
            new.delete()
            return redirect('news-add')

    context = {
        'new': new,
        'form':NewsForm,
    }
    return render(request,'admin_panel/news-edit.html',context)