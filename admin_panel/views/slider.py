from django.shortcuts import render, redirect,get_object_or_404
from front.models import *
from django.contrib.auth.decorators import login_required
from django.utils.translation import *

@login_required
def SliderAdd(request):
    current_language = get_language()
    sliders = SliderInfo.objects.all()
    if request.method == 'POST':
        name = request.POST['name']
        text = request.POST['text']
        img = request.FILES.get('img')
        back = request.FILES.get('back')
        if current_language == 'kk':
            SliderInfo.objects.create(name_kk=name,text_kk=text,img=img,background_img=back)
            return redirect('slider-add')
        if current_language == 'en':
            SliderInfo.objects.create(name_en=name,text_en=text,img=img,background_img=back)
            return redirect('slider-add')
        if current_language == 'ru':
            SliderInfo.objects.create(name_ru=name,text_ru=text,img=img,background_img=back)
            return redirect('slider-add')
    context={
        'sliders':sliders,
    }
    return render(request,'admin_panel/slider-add.html',context)

@login_required
def SliderEdit(request,slider_id):
    current_language = get_language()
    slider = get_object_or_404(SliderInfo,pk=slider_id)
    if request.method == 'POST':
        type = request.POST['form']
        if type == 'edit':
            name = request.POST['name']
            text = request.POST['text']
            img = request.FILES.get('img')
            background_img = request.FILES.get('back')
            if img == None and background_img == None:
                if current_language == 'kk':
                    slider.name_kk = name
                    slider.text_kk = text
                    slider.save()
                    return redirect('slider-edit',slider_id)
                if current_language == 'en':
                    slider.name_en = name
                    slider.text_en = text
                    slider.save()
                    return redirect('slider-edit',slider_id)
                if current_language == 'ru':
                    slider.name_ru = name
                    slider.text_ru = text
                    slider.save()
                    return redirect('slider-edit',slider_id)
            elif img == None and background_img != None:
                if current_language == 'kk':
                    slider.name_kk = name
                    slider.text_kk = text
                    slider.background_img = background_img
                    slider.save()
                    return redirect('slider-edit', slider_id)
                if current_language == 'en':
                    slider.name_en = name
                    slider.text_en = text
                    slider.background_img = background_img
                    slider.save()
                    return redirect('slider-edit', slider_id)
                if current_language == 'ru':
                    slider.name_ru = name
                    slider.text_ru = text
                    slider.background_img = background_img
                    slider.save()
                    return redirect('slider-edit', slider_id)
            elif img != None and background_img == None:
                if current_language == 'kk':
                    slider.name_kk = name
                    slider.text_kk = text
                    slider.img = img
                    slider.save()
                    return redirect('slider-edit', slider_id)
                if current_language == 'en':
                    slider.name_en = name
                    slider.text_en = text
                    slider.img = img
                    slider.save()
                    return redirect('slider-edit', slider_id)
                if current_language == 'ru':
                    slider.name_ru = name
                    slider.text_ru = text
                    slider.img = img
                    slider.save()
                    return redirect('slider-edit', slider_id)
            elif img != None and background_img != None:
                if current_language == 'kk':
                    slider.name_kk = name
                    slider.text_kk = text
                    slider.img = img
                    slider.background_img = background_img
                    slider.save()
                    return redirect('slider-edit', slider_id)
                if current_language == 'en':
                    slider.name_en = name
                    slider.text_en = text
                    slider.img = img
                    slider.background_img = background_img
                    slider.save()
                    return redirect('slider-edit', slider_id)
                if current_language == 'ru':
                    slider.name_ru = name
                    slider.text_ru = text
                    slider.img = img
                    slider.background_img = background_img
                    slider.save()
                    return redirect('slider-edit', slider_id)
        if type == 'delete':
            slider.delete()
            return redirect('slider-add')
    context ={
        'slider':slider,
    }
    return render(request,'admin_panel/slider-edit.html',context)