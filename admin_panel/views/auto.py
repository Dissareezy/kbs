from django.shortcuts import render,redirect,get_object_or_404
from auto.models import *
from django.contrib.auth.decorators import login_required
@login_required
def autoSliderView(request):
    sliders = AutoSlider.objects.all()
    if request.method == 'POST':
        name = request.POST['name']
        text = request.POST['text']
        img = request.FILES.get('img')
        back = request.FILES.get('back')
        AutoSlider.objects.create(name=name, text=text, img=img, background_img=back)
        return redirect('auto-slider-add')
    context = {
        'sliders': sliders,
    }
    return render(request,'admin_panel/auto/slider-add.html',context)
@login_required
def autoSliderEditView(request,slider_id):
    slider = get_object_or_404(AutoSlider, pk=slider_id)
    if request.method == 'POST':
        type = request.POST['form']
        if type == 'edit':
            name = request.POST['name']
            text = request.POST['text']
            img = request.FILES.get('img')
            background_img = request.FILES.get('back')
            if img == None and background_img == None:
                slider.name = name
                slider.text = text
                slider.save()
                return redirect('auto-slider-edit', slider_id)
            elif img == None and background_img != None:
                slider.name = name
                slider.text = text
                slider.background_img = background_img
                slider.save()
                return redirect('auto-slider-edit', slider_id)
            elif img != None and background_img == None:
                slider.name = name
                slider.text = text
                slider.img = img
                slider.save()
                return redirect('auto-slider-edit', slider_id)
            elif img != None and background_img != None:
                slider.name = name
                slider.text = text
                slider.img = img
                slider.background_img = background_img
                slider.save()
                return redirect('auto-slider-edit', slider_id)
        if type == 'delete':
            slider.delete()
            return redirect('auto-slider-add')
    context = {
        'slider': slider,
    }
    return render(request,'admin_panel/auto/slider-edit.html',context)


@login_required
def AutoContactsView(request):
    contacts = Contact.objects.all()
    address = Address.objects.all()
    phone = Phone.objects.all()
    email = Email.objects.all()
    worktime = Worktime.objects.all()
    if request.method == 'POST':
        type = request.POST['type']
        if type == 'address':
            address = request.POST['address']
            Address.objects.create(address=address)
            return redirect('admin-contacts')
        if type == 'phone':
            phone = request.POST['phone']
            Phone.objects.create(phone=phone)
            return redirect('admin-contacts')
        if type == 'email':
            email = request.POST['email']
            Email.objects.create(email=email)
            return redirect('admin-contacts')
        if type == 'worktime':
            holidaytime = request.POST['worktime']
            Worktime.objects.create(holidaytime=holidaytime)
            return redirect('admin-contacts')
    context = {
        'contacts': contacts,
        'address': address,
        'email': email,
        'phone': phone,
        'worktime': worktime,
    }
    return render(request, 'admin_panel/admin-contacts.html', context=context)

@login_required
def AutoConctacsEditView(request,contact_id):
    if request.method == 'POST':
        type = request.POST['form']
        if type == 'address-edit':
            address = request.POST['address']
            ad_obj = get_object_or_404(Address,pk=contact_id)
            ad_obj.address=address
            ad_obj.save()
            return redirect('admin-contacts')
        if type == 'phone-edit':
            phone = request.POST['phone']
            ad_obj = get_object_or_404(Phone,pk=contact_id)
            ad_obj.phone=phone
            ad_obj.save()
            return redirect('admin-contacts')
        if type == 'email-edit':
            email = request.POST['email']
            ad_obj = get_object_or_404(Email,pk=contact_id)
            ad_obj.email=email
            ad_obj.save()
            return redirect('admin-contacts')
        if type == 'worktime-edit':
            worktime = request.POST['holidaytime']
            ad_obj = get_object_or_404(Worktime,pk=contact_id)
            ad_obj.holidaytime=worktime
            ad_obj.save()
            return redirect('admin-contacts')
        if type == 'address-delete':
            address_pk = request.POST['address_pk']
            address = get_object_or_404(Address,pk=address_pk)
            address.delete()
        if type == 'phone-delete':
            phone_pk = request.POST['phone_pk']
            phone = get_object_or_404(Phone,pk=phone_pk)
            phone.delete()
        if type == 'email-delete':
            email_pk = request.POST['email_pk']
            email = get_object_or_404(Email,pk=email_pk)
            email.delete()
        if type == 'worktime-delete':
            worktime_pk = request.POST['worktime_pk']
            email = get_object_or_404(Worktime,pk=worktime_pk)
            email.delete()

    context = {
    }
    return redirect('admin-contacts')