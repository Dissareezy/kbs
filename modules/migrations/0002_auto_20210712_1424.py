# Generated by Django 3.0.4 on 2021-07-12 08:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modules', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='modulesslider',
            name='background_img',
            field=models.ImageField(default=1, upload_to='modulesslider/img/back/'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='modulesslider',
            name='name',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='modulesslider',
            name='img',
            field=models.ImageField(upload_to='modules-slider/img/'),
        ),
    ]
