from modeltranslation.translator import register, TranslationOptions
from .models import *

@register(ModulesSlider)
class ModulesSliderTranslationOptions(TranslationOptions):
    fields = ('name','text')
