from django.db import models

class ModulesSlider(models.Model):
    name = models.CharField(max_length=255)
    text = models.TextField(default='')
    img = models.ImageField(upload_to='modules-slider/img/')
    background_img = models.ImageField(upload_to='modulesslider/img/back/')



class ModContacts(models.Model):
    address = models.TextField(default="")
    email = models.TextField(default="")

class ModPhone(models.Model):
    phone = models.TextField(default="")

class ModAddress(models.Model):
    address = models.TextField(default="")

class ModEmail(models.Model):
    email = models.TextField(default="")

class ModGallery(models.Model):
    img = models.ImageField(upload_to='modules/gallery/')
    date = models.DateField(auto_now_add=True)
    description = models.TextField(default="")

class MdoTrust(models.Model):
    logo = models.ImageField(upload_to="modules/trust/images/")